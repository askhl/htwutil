import taskblaster as tb


@tb.workflow
class SubWorkflow:
    number = tb.var()
    second_number = tb.var()

    @tb.task
    def ok(self):
        return tb.node('ok', number=self.number)

    @tb.task
    def ok2(self):
        return tb.node('ok', number=self.second_number)


@tb.workflow
class Workflow:
    number = tb.var()
    max_num = tb.var()

    @tb.task
    def ok(self):
        return tb.node('ok', number=self.number)

    @tb.task
    def plus_two(self):
        return tb.node('plus_two', number=self.ok)

    @tb.task
    def might_fail1(self):
        return tb.node('cond_fail', number=self.ok, max_num=self.max_num)

    @tb.task
    def might_fail2(self):
        return tb.node('cond_fail', number=self.plus_two, max_num=self.max_num)

    @tb.subworkflow
    def subwf(self):
        return SubWorkflow(number=self.ok, second_number=self.might_fail2)


def workflow(runner):
    runner.run_workflow(Workflow(number=1, max_num=2))
