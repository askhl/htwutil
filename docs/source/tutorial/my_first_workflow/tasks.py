def ok(number):
    return number


def plus_two(number):
    return number + 2


def cond_fail(number, max_num):
    if number > max_num:
        raise ValueError(f'{number} > {max_num}')
    else:
        return number
