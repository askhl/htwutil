import taskblaster as tb


@tb.workflow
class Workflow:
    number = tb.var()

    @tb.task
    def task1(self):
        return tb.node('plus_two', number=self.number)


def workflow(runner):
    runner.run_workflow(Workflow(number=1))
