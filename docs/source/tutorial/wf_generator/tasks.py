import taskblaster as tb


# -- first_part_start
def ok(number):
    return number


def plus_two(number):
    return [n for n in range(number + 2)]


# -- first_part_end


@tb.dynamical_workflow_generator_task
def generate_wfs_from_list(inputs):
    for inp in inputs:
        wf = SubWorkflow(number=inp)
        name = f'wf_{inp}'
        yield name, wf


@tb.workflow
class SubWorkflow:
    number = tb.var()

    @tb.task
    def generated_task1(self):
        return tb.node('ok', number=self.number)

    @tb.task
    def generated_task2(self):
        return tb.node('ok', number=self.number)
