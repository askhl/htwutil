import taskblaster as tb


@tb.workflow
class Workflow:
    number = tb.var()

    @tb.task
    def task1(self):
        return tb.node('plus_two', number=self.number)

    # -- start_display
    @tb.dynamical_workflow_generator(
        {
            'results': '*/*',
            'results_task1': '*/generated_task1',
            'results_task2': '*/generated_task2',
        }
    )
    def generated_wfs(self):
        return tb.node('generate_wfs_from_list', inputs=self.task1)

    # -- end_display


def workflow(runner):
    runner.run_workflow(Workflow(number=1))
