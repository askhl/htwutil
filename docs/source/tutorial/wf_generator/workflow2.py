import taskblaster as tb


@tb.workflow
class Workflow:
    number = tb.var()

    @tb.task
    def task1(self):
        return tb.node('plus_two', number=self.number)

    @tb.dynamical_workflow_generator({'results': '*/*'})
    def generated_wfs(self):
        return tb.node('generate_wfs_from_list', inputs=self.task1)


def workflow(runner):
    runner.run_workflow(Workflow(number=1))
