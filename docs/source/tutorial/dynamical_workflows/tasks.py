def A():
    return 1


def B(a):
    return a > 1


def C(a):
    return a + 2


def D(a):
    return a + 3


def E(a):
    return a + 4
