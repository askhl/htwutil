import taskblaster as tb


@tb.workflow
class CollatzIteration:
    number = tb.var()

    @tb._if(true='even_branch', false='odd_branch')
    @tb.task
    def IsEven(self):
        return tb.node('is_even', number=self.number)

    @tb.branch('even_branch')
    @tb.jump('result')
    @tb.task
    def even_task(self):
        return tb.node('divide_by_two', number=self.number)

    @tb.branch('odd_branch')
    @tb.jump('result')
    @tb.task
    def odd_task(self):
        return tb.node('three_n_plus_one', number=self.number)

    @tb.branch('result')
    @tb.fixedpoint
    @tb.task
    def result(self):
        return tb.node(
            'define',
            obj=self.Phi(even_branch=self.even_task, odd_branch=self.odd_task),
        )


@tb.workflow
class CollatzSequence:
    number = tb.var()

    @tb.branch('entry', loop=True)
    @tb.subworkflow
    def iteration(self):
        return CollatzIteration(
            number=self.Phi(default=self.number, entry=self.iteration.result)
        )

    @tb.branch('entry', loop=True)
    @tb.task
    def sequence(self):
        return tb.node(
            'sequence',
            sequence=self.Phi(default=[self.number], entry=self.sequence),
            number=self.iteration.result,
        )

    @tb.branch('entry', loop=True)
    @tb._if(true='finish', false='entry')
    @tb.task
    def stop_iteration(self):
        return tb.node('stop_iteration', number=self.iteration.result)

    @tb.branch('finish')
    @tb.task
    def result(self):
        return tb.node('define', obj=self.sequence)


def workflow(runner):
    runner.run_workflow(CollatzSequence(number=5))
