def is_even(number):
    return number % 2 == 0


def divide_by_two(number):
    return number // 2


def three_n_plus_one(number):
    return 3 * number + 1


def results(number, sequence):
    return number, sequence + [number]


def sequence(sequence, number):
    return [*sequence, number]


def stop_iteration(number):
    return number == 1
