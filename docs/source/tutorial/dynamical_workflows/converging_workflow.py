import taskblaster as tb


@tb.workflow
class CollatzIteration:
    number = tb.var()
    sequence = tb.var()

    @tb._if(true='even_branch', false='odd_branch')
    @tb.task
    def IsEven(self):
        return tb.node('is_even', number=self.number)

    @tb.branch('even_branch')
    @tb.jump('results')
    @tb.task
    def even_task(self):
        return tb.node('divide_by_two', number=self.number)

    @tb.branch('odd_branch')
    @tb.jump('results')
    @tb.task
    def odd_task(self):
        return tb.node('three_n_plus_one', number=self.number)

    @tb.branch('results')
    @tb.task
    def gather_result(self):
        return tb.node(
            'results',
            number=self.Phi(
                even_branch=self.even_task, odd_branch=self.odd_task
            ),
            sequence=self.sequence,
        )


def workflow(runner):
    runner.run_workflow(CollatzIteration(number=5, sequence=[]))
