import taskblaster as tb


@tb.workflow
class Workflow:
    greeting = tb.var(default='hello')
    whom = tb.var()
    username = tb.var(default='User')

    @tb.task
    def hello(self):
        return tb.node('greet', greeting=self.greeting, whom=self.whom)

    @tb.task
    def hello_user(self):
        return tb.node('greet', greeting=self.greeting, whom=self.username)


def workflow(runner):
    runner.run_workflow(Workflow(whom='new world', username='Tara'))
