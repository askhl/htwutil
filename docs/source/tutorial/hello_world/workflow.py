import taskblaster as tb


@tb.workflow
class Workflow:
    greeting = tb.var(default='hello')
    whom = tb.var()

    @tb.task
    def hello(self):
        return tb.node('greet', greeting=self.greeting, whom=self.whom)


def workflow(runner):
    runner.run_workflow(Workflow(whom='world'))
