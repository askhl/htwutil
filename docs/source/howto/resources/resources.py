resources = {
    'alberich1': {'tags': {'hello'}},
    'alberich2': {'required_tags': {'eggs', 'spam'}},
    'mq_alberich1': {'tags': {'hello'}, 'resources': '1:alberich1:1m'},
    'superworker': {'tags': {'hello', 'highmem', 'gpu'}},
    'parallel_hello': {'resources': '40:hello:1h'},
    'parallel': {'resources': '40:1h'},
    'lowmem': {'tags': {'memoryerror'}, 'resources': '1:alberich1:1m'},
    'highmem': {},
}
