#!/bin/bash -l
#SBATCH -J <job_name>
#SBATCH --output=<job_output>-%j.out
#SBATCH --time=0-01:00:00
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --partition=partition_name

source /active/python/env/command
cd /path/to/root/tree

srun tb run .
