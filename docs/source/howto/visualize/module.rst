.. _how_to_visualize_workflows:

How to visualize workflows?
=====================================================

Taskblaster provides (experimental, not supporting yet all features) visualization
of workflows.

.. tbinit:: visualize
.. tbhiddenshellcommand:: tb init
.. tbshellcommand:: tb view-workflow --help

We can for example use the `workflow.py` from the :ref:`while loop <how_to_while_loop>` example.

.. tbfile:: ../while_loop/workflow.py workflow.py
.. tbshellcommand:: tb view-workflow -f workflow.py -o while_loop_workflow.html WhileLoopWorkflow

.. tbviewworkflow:: while_loop_workflow.html

You can view the generated visualization :download:`here <../../_static/while_loop_workflow.html>`.

Customization
-------------

The ``tb view-workflow`` command also accepts custom style options. To get latest default style, one can write the
following. 


.. tbshellcommand:: python -c "from taskblaster.view_workflow_html import DEFAULT_STYLE;import json;print(json.dumps(DEFAULT_STYLE, indent=4))" | tee default_style.json

Save this style file to default_style.json to run the tutorial further. It contains all the defaults and paramters that can be adjusted
for the workflow visualization, and thus acts as a good skeleton for complete control of workflow visualization.
Unfortunately, at this stage, it is impossible to make publication quality plots for complicated workflows,
but nevertheless they might help to understand the parameter routings, subworkflow structure, and branching in some cases.

To utilize the style file, one can pass it as ``-s`` parameter.

.. tbshellcommand:: tb view-workflow -f workflow.py -s default_style.json -o styled_workflow.html WhileLoopWorkflow

.. tbviewworkflow:: styled_workflow.html


You can view the generated visualization with the default style :download:`here <../../_static/styled_workflow.html>`. which should be
identical to the previous one.

.. tbshellcommand:: cmp while_loop_workflow.html styled_workflow.html && echo "Files identical!"

