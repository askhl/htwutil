import taskblaster as tb


@tb.workflow
class WhileLoopWorkflow:
    number = tb.var()

    @tb.branch('entry', loop=True)
    @tb.task
    def iterate(self):
        return tb.node(
            'iterate_task',
            number=self.Phi(default=self.number, entry=self.iterate),
        )

    @tb.branch('entry', loop=True)
    @tb._if(false='entry')
    @tb.task
    def converged(self):
        return tb.node('converged_task', number=self.iterate)


def workflow(rn):
    rn.run_workflow(WhileLoopWorkflow(number=3))
