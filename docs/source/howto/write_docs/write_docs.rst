=================================
Writing Taskblaster documentation
=================================

Taskblaster documentation can be found in the ``docs/`` folder.
All of the source rst files can be found at ``docs/source/`` folder.
You need to write ``make html`` and then you can open the documentation with a
browser, eg. ``firefox build/html/index.html``.

There are three custom spinx commands that one can utilize to create Taskblaster
tutorials.  A tutorial will likely need to run commands on a Taskblaster project.
Initialize a project with a unique name::

    ..tbinit:: my_example


This will create a temporary directory where the sphinx builder will run taskblaster.
You can run any shell command inside that place::

    ..tbshellcommand:: tb info

The ``tbshellcommand`` directive will run any shell command in the directory.
Be careful, you can
cause a lot of damage with shell commands which for example remove files.
The shell command will convert ansi-color codes to html-spans with proper css
to account for displaying shell (If you get a build error, your color has not been
implemented yet, create an issue).  Finally,

::

    ..tbfile:: workflow.py


Will read the file ``workflow.py`` from the current
folder where the ``rst`` file is located,
and write it to the temporary folder.
Now you may use it in the later shell commands, for example when running workflows.
