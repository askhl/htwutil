External files
==============

External files can be introduced as

::

    class Workflow:
        database_file = tb.input()

        @tb.external_file
        def database(self):
            return self.database_file

The external_file will appear as a task, upon running this will copy the external file to the task folder database, and hash it.
The output of the task will be HashedExternalFile object, which also will contain the digest of the file.

TODO: In addition, it will introduce a verification hook, which will run each time the workflow is run.


The external file can be used as follows

::

    class Workflow:
        ...

        @tb.task
        def totree(self):
            return tb.node('tasks.totree', database=self.database)

    ...
    def totree(database):
        from ase.db import connect
        with connect(database.path):
            ...

