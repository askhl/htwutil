import taskblaster as tb


@tb.workflow
class Workflow:
    greeting = tb.var()

    @tb.task
    def hello(self):
        return tb.node('write_greeting', greeting=self.greeting)

    @tb.task
    def read_hello(self):
        return tb.node('read_and_return_greeting', greeting_path=self.hello)


def workflow(runner):
    runner.run_workflow(Workflow(greeting='hello world'))
