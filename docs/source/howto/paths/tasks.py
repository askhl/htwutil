from pathlib import Path


def write_greeting(greeting):
    with open('output.txt', 'w') as fd:
        fd.write(greeting)
    return Path('output.txt')


def read_and_return_greeting(greeting_path):
    with open(greeting_path, 'r') as fd:
        lines = fd.readlines()
    return lines
