Paths and output files
======================

To assure that the dependency between different tasks is handled correctly it is essential that all outputs from tasks are provided as an explicit output from the Python function that represents the task. However, there are cases where the result has to be written to output files and cannot be encoded as a single output variable
(see e.g. the ground state task in the section :ref:`my-reference-label`).
In such a case the path to the file can be returned as a `pathlib.Path` object. The system then automatically assures that the path resolves to the correct directory. Below we provide a simple example of tasks that read/write output files.

First create a taskblaster repository

.. tbinit:: paths_section

.. tbshellcommand:: tb init

An example of tasks that read/write output files as paths is given by the following workflow

.. tbfile:: workflow.py

.. literalinclude:: workflow.py

with the tasks defined in `tasks.py`

.. tbfile:: tasks.py

.. literalinclude:: tasks.py

If you run the workflow

.. tbshellcommand:: tb workflow workflow.py

.. tbshellcommand:: tb run tree

you can view how the output is encoded using the `tb view` command:

.. tbshellcommand:: tb view tree
