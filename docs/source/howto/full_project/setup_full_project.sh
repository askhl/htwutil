#!/bin/bash

python -m venv tb_demo_venv
source tb_demo_venv/bin/activate

cd tb_demo_venv

mkdir tb_demo
cd tb_demo

cat << EOF > pyproject.toml
[project]
name = 'tb_demo'
requires-python = '>=3.8'
dependencies = ['taskblaster']
version = '0.1'

[build-system]
requires = ['setuptools']
build-backend = 'setuptools.build_meta'
description = 'workflow code for relaxations and possibly other things'

[tool.setuptools.packages.find]
where = ['.']
include = ['tb_demo']
EOF

mkdir tb_demo
cd tb_demo

cat << EOF > __init__.py
import taskblaster as tb
from taskblaster.repository import Repository

def tb_init_repo(root):
    # This is the most plain module initialization code.
    # For custom encoders/decoders and MPI support, further tutorials are coming up.
    return Repository(root)
EOF

mkdir mysubmodule
cd mysubmodule
cat << EOF >> __init__.py
import taskblaster as tb

class MyCustomObject:
    def __init__(self, parameter):
        self.parameter = parameter

    def tb_encode(self):
        return {'my_stored_parameter': self.parameter}

    @classmethod
    def tb_decode(cls, dct: dict):
        return cls(parameter=dct['my_stored_parameter'])

    def calculate(self):
        return f'Calculated using {self.parameter}.'

@tb.workflow
class MyWorkflow:
    @tb.task
    def create_object(self):
        return tb.node('tb_demo.mysubmodule.tasks.create_object')

    @tb.task
    def use_object(self):
        return tb.node('tb_demo.mysubmodule.tasks.use_object', obj=self.create_object)
EOF

cat << EOF > tasks.py
from tb_demo.mysubmodule import MyCustomObject

def create_object():
    return MyCustomObject(parameter=42)

def use_object(obj):
    return obj.calculate()
EOF

cd ..

cat << EOF > main_workflow.py
from tb_demo.mysubmodule import MyWorkflow

def workflow(runner):
    runner.run_workflow(MyWorkflow())
EOF

cd ..

pip install -e .
cd ..
cd ..


