#!/bin/bash

python -m venv asedemo_venv
source asedemo_venv/bin/activate

cd asedemo_venv

mkdir asedemo
cd asedemo

cat << EOF > pyproject.toml
[project]
name = 'asedemo'
requires-python = '>=3.8'
dependencies = ['taskblaster', 'ase']
version = '0.1'

[build-system]
requires = ['setuptools']
build-backend = 'setuptools.build_meta'
description = 'workflow code for relaxations and possibly other things'

[tool.setuptools.packages.find]
where = ['.']
include = ['asedemo']
EOF

mkdir asedemo
cd asedemo

cat << EOF > __init__.py
from taskblaster.repository import Repository
from taskblaster.storage import JSONCodec


class ASECodec(JSONCodec):
    def encode(self, obj):
        from ase.io.jsonio import default
        return default(obj)

    def decode(self, dct):
        from ase.io.jsonio import object_hook
        return object_hook(dct)


class AseDemoRepository(Repository):
    """Allows further customizations, described in further How to's in the future.
    """

# Magical "initializer" for taskblaster repository using existing
# ase encoder. Taskblaster imports this and uses it to initialize
# any repository.
def tb_init_repo(root):
    return AseDemoRepository(
        root, usercodec=ASECodec())
EOF

mkdir mysubmodule
cd mysubmodule
cat << EOF >> __init__.py
import taskblaster as tb

@tb.workflow
class MyWorkflow:
    symbol = tb.var()

    @tb.task
    def atoms(self):
        return tb.node('asedemo.mysubmodule.tasks.create_atoms', symbol=self.symbol)

    @tb.task
    def relaxed_atoms(self):
        return tb.node('asedemo.mysubmodule.tasks.relax', atoms=self.atoms)

EOF

cat << EOF > tasks.py
from ase.optimize.cellawarebfgs import CellAwareBFGS
from ase.filters import FrechetCellFilter
from ase.calculators.emt import EMT
from ase.build import bulk


def create_atoms(symbol):
    atoms = bulk(symbol)
    # Introduce some deformation to get more relax steps for the show
    atoms.set_cell(1.2 * atoms.get_cell())
    return atoms

def relax(atoms):
    atoms.calc = EMT()
    relax = CellAwareBFGS(FrechetCellFilter(atoms))
    relax.run()
    return {'relaxation_result': atoms,
            'total_steps': relax.nsteps}
EOF

cd ..

cat << EOF > main_workflow.py
from asedemo.mysubmodule import MyWorkflow

def workflow(runner):
    for metal in ['Cu','Ag','Fe','Ni']:
        rn = runner.with_subdirectory(metal)
        rn.run_workflow(MyWorkflow(symbol=metal))
EOF

cd ..

pip install -e .
cd ..
cd ..


