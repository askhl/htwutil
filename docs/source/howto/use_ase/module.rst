How to set up a large scale project with Taskblaster using ASE (Atomic Simulation Environment)?
===============================================================================================

Read first :ref:`how_to_full_project`, which will explain the basic building blocks
of a Python project. Here, we will focus on creating a new project, which contains those parts,
but also adds ASE encoders and decoders, such that we store ASE objects (Atoms, BandStructure, etc.)

.. tbinit:: full_project_ase

Run this :download:`script <setup_full_project_ase.sh>` which will create the project files,
and a virtual environement, and install the project to that virtual environment.

.. tbfile:: setup_full_project_ase.sh 

.. tbshellcommand:: chmod +x setup_full_project_ase.sh && ./setup_full_project_ase.sh

You should have following files now in a package called asedemo in your virtual environment.

.. tbshellcommand:: cd asedemo_venv/asedemo && find . | grep -v "egg-info"

There is a Python module called ``asedemo`` in the folder with the same name.
We can now initialize a Taskblaster repo using ``tb init asedemo``.
We can do this command from anywhere, since we always can locate the installed module ``asedemo``.

.. warning::

    Before writing ``tb init asedemo`` make sure to go to a fresh folder (do not create a Taskblaster
    repository to your home folder, or to your virtual environment ``asedemo_venv``-folder).

We can now activate the virtual environment.

.. tbvenv:: asedemo_venv/bin/activate

.. tbshellcommand:: tb init asedemo

We can now execute our workflow, which now executes a simple workflow in various folders.

.. tbshellcommand:: tb workflow asedemo_venv/asedemo/asedemo/main_workflow.py

we can now run the tasks, and we observe the relaxation of atoms.

.. tbshellcommand:: tb run .

We can check the outcome by listing the outputs. We on purpose included an error to task (missing EMT potential)
to show how that would show up.

.. tbshellcommand:: tb ls

We can examine the output of the task returning the relaxed atoms using ``tb view`` command,

.. tbshellcommand:: tb view tree/Ni/relaxed_atoms

or we can see how the ``ase.Atoms`` objects are encoded into the output dictionary

.. tbshellcommand:: cat tree/Ni/relaxed_atoms/output.json

.. tbvenv:: deactivate
