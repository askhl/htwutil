def initialize(number):
    # Preprocessing of possible inputs
    return number


def converged_task(number):
    # This task could determine whether atoms wave been relaxed?
    return number < 1


def iterate_task(number):
    # This could relax atoms, and return the relaxed atoms
    return number - 1


def depend_on_result(number):
    # This task depends on the final result of the while loop
    return number
