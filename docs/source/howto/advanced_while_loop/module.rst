How to write an advanced while loop?
====================================

.. tbinit:: howto_advwhileloop

See how to write a while loop *how to* for a simplest possible while loop. Here, we will consider a more complicated case with possible
initialization.

We begin from empty repo.

.. tbshellcommand:: tb init

Create a following ``workflow.py``

.. tbfile:: workflow.py

.. literalinclude:: workflow.py

and a following ``tasks.py``

.. tbfile:: tasks.py

.. literalinclude:: tasks.py

We note, that we will start with entry branch, and then we will immediately make a non-conditional jump to the ``whilebranch``.
Now, we have an ``_if`` task, which either jumps back to this branch, or to ``final`` branch.
We have defined the ``final`` branch task as fixedpoint, which allows us to reference it from outside of this workflow,
even the branch has now been executed yet.

We can iterate the workflow few times and observe the results. We iterate the workflow 4 times, run the created tasks, and observe the outputs.

.. tbshellcommand:: tb workflow workflow.py && tb run . >/dev/null && tb ls --sort=topo -c sirITfo
.. tbshellcommand:: tb workflow workflow.py && tb run . >/dev/null && tb ls --sort=topo -c sirITfo
.. tbshellcommand:: tb workflow workflow.py && tb run . >/dev/null && tb ls --sort=topo -c sirITfo
.. tbshellcommand:: tb workflow workflow.py && tb run . >/dev/null && tb ls --sort=topo -c sirITfo

.. tbfile:: ../../tutorial/dynamical_workflows/converging_style.json
.. tbhiddenshellcommand:: tb view-workflow -s converging_style.json -f workflow.py -o adv_while_loop_workflow.html WhileLoopWorkflow
.. tbviewworkflow:: adv_while_loop_workflow.html

To view an experimental automatically generated workflow diagram of the workflow here, click :download:`here <../../_static/adv_while_loop_workflow.html>`.

This structure is an example of a more advanced for loop. 
