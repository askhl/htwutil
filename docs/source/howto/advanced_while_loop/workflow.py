import taskblaster as tb


@tb.workflow
class WhileLoopWorkflow:
    number = tb.var()

    @tb.jump('whilebranch')
    # @tb.branch('entry') is automatic, so skipped here
    @tb.task
    def initialize(self):
        return tb.node('initialize', number=self.number)

    @tb.branch('whilebranch', loop=True)
    @tb.task
    def iterate(self):
        return tb.node(
            'iterate_task',
            number=self.Phi(entry=self.initialize, whilebranch=self.iterate),
        )

    @tb.branch('whilebranch', loop=True)
    @tb._if(false='whilebranch', true='final')
    @tb.task
    def converged(self):
        return tb.node('converged_task', number=self.iterate)

    @tb.branch('final')
    @tb.fixedpoint
    @tb.task
    def result(self):
        return tb.node('define', obj=self.iterate)


def workflow(rn):
    rn.run_workflow(WhileLoopWorkflow(number=3))
