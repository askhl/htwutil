============
Installation
============

Install latest release of taskblaster using pip::

  pip install taskblaster

For development or bleeding-edge features, you will need the developer
version from Gitlab.  Go to a suitable directory and clone the code::

  git clone https://gitlab.com/taskblaster/taskblaster.git

If you cloned the code, you may wish to perform an editable installation.
Users may want an ordinary installation whereas developers would want
a developer installation.  For a minimal installation, run::

  pip install --editable taskblaster/

Above, ``taskblaster/`` must be the directory that taskblaster was cloned to.
For a developer installation, include extra optional dependencies::

  pip install --editable taskblaster[test,docs]
