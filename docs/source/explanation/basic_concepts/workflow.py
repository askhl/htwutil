from pathlib import Path

from ase import Atoms

import taskblaster as tb


@tb.workflow
class RelaxGsWorkflow:
    atoms = tb.var()
    calc_params_relax = tb.var()
    calc_params_gs = tb.var()
    optimizer_params = tb.var()

    @tb.task
    def relax(self) -> Atoms:
        return tb.node(
            'relax',
            atoms=self.atoms,
            calc_params=self.calc_params_relax,
            optimizer_params=self.optimizer_params,
        )

    @tb.task
    def groundstate(self) -> Path:
        return tb.node(
            'groundstate', atoms=self.relax, calc_params=self.calc_params_gs
        )


# -- literalinclude-marker


@tb.workflow
class MyWorkflow:
    atoms = tb.var()
    calc_params_relax = tb.var()
    calc_params_gs = tb.var()
    optimizer_params = tb.var()

    @tb.subworkflow
    def relax_and_gs(self):
        return RelaxGsWorkflow(
            atoms=self.atoms,
            calc_params_relax=self.calc_params_relax,
            calc_params_gs=self.calc_params_gs,
            optimizer_params=self.optimizer_params,
        )

    @tb.task
    def postprocess(self):
        return tb.node('postprocess', gs=self.relax_and_gs.groundstate)
