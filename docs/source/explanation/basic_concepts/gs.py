from pathlib import Path

from ase import Atoms
from gpaw import GPAW

import taskblaster as tb


@tb.mpi
def groundstate(atoms: Atoms, calc_params: dict, mpi: tb.mpi) -> Path:
    """
    Task to perform an electronic ground state calculation using GPAW.

    :param atoms: ASE Atoms object.
    :param calc_params: A dictionary containing the calculator input
        parameters.
    :param mpi: TaskBlaster MPI object.
    :return: Path to gs file
    """

    calc = GPAW(**calc_params, communicator=mpi.comm)
    atoms.calc = calc
    atoms.get_potential_energy()
    calc.write('gs.gpw')
    return Path('gs.gpw')
