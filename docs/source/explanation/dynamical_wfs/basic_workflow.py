import taskblaster as tb


@tb.workflow
class StaticWorkflow:
    @tb.task
    def taskA(self):
        return tb.node('taskA')

    @tb.task
    def taskB(self):
        return tb.node('taskB', param_A=self.taskA)

    @tb.task
    def taskC(self):
        return tb.node('taskC', param_A=self.taskA)
