import taskblaster as tb


@tb.workflow
class DynamicWorkflow:
    @tb.task
    def A(self):
        return tb.node('A')

    @tb._if(true='Cbranch', false='Dbranch')
    @tb.task
    def B(self):
        return tb.node('B', a=self.A)

    @tb.branch('Cbranch')
    @tb.task
    def C(self):
        return tb.node('C', a=self.A)

    @tb.branch('Dbranch')
    @tb.task
    def D(self):
        return tb.node('D', a=self.A)


def workflow(runner):
    runner.run_workflow(DynamicWorkflow())
