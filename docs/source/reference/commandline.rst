Command-line reference
======================

.. tbinit:: _tempdir_commandline_reference

Taskblaster provides a command-line interface based on a single
toplevel command, normally installed as ``tb``, with a number of
subcommands.  Each command has a ``--help`` page.  The
help page of the toplevel command is:

.. tbshellcommand:: tb --help


The page exhaustively lists the documentation for to all
subcommands and their options across all ``--help`` pages.

.. click:: taskblaster.cli:tb
  :prog: tb
  :nested: full
