Taskblaster
===========

Taskblaster is a Python utility for executing high-throughput
computational workflows.

Links:

 * Documentation: https://taskblaster.readthedocs.io/
 * Releases: https://pypi.org/project/taskblaster/
 * Chat: https://app.element.io/#/room/#taskblaster:matrix.org
 * Development: https://gitlab.com/taskblaster/taskblaster

Installation
------------

To install latest release of Taskblaster, use::

  pip install taskblaster

License
-------

Copyright Taskblaster developers, 2022.

This project is released under GNU GPLv3+, see the LICENSE file.
