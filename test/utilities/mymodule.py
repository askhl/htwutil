import taskblaster as tb
from taskblaster.repository import Repository


@tb.workflow
class OutOfOrderAccess:
    @tb.branch('entry')
    @tb.task
    def start_task(self):
        return tb.node('define', obj=self.unvisited_task)

    @tb.branch('unreachable')
    @tb.task
    def unvisited_task(self):
        return tb.node('define', obj=1)


@tb.workflow
class OutOfOrderAccess2:
    @tb.branch('entry')
    @tb.task
    def start_task(self):
        return tb.node('define', obj=2)

    @tb.branch('entry')
    @tb._if(true='final', false='unreachable')
    @tb.task
    def if_task(self):
        return tb.node('define', obj=True)

    @tb.branch('unreachable')
    @tb.task
    def unvisited_task(self):
        return tb.node('define', obj=1)

    @tb.branch('final')
    @tb.task
    def referencing_task(self):
        return tb.node('define', obj=self.unvisited_task)


@tb.workflow
class SubworkflowWithCall:
    input_value = tb.var()

    @tb.task
    def test(self):
        return tb.node(
            myassert,
            a=self.input_value.test_function(
                self.input_value.storage, 'u', 'v'
            ).storage,
            b='asd567asd567uv',
        )

    @tb.task
    def return_object_subworkflow(self):
        return tb.node(return_object_task)


class MyCustomObject:
    def __init__(self, storage):
        print(f'MyCustomObject(storage={storage})')
        self.storage = storage
        self.test_property_called = False

    def tb_encode(self):
        return {'storage': self.storage}

    @classmethod
    def create_an_object(cls, storage):
        return cls(storage)

    @classmethod
    def tb_decode(cls, dct):
        return cls(storage=dct['storage'])

    @property
    def test_property(self):
        self.test_property_called = True
        return self

    def test_function(self, a, b, c=None):
        return MyCustomObject(storage=self.storage + a + b + c)


def return_object_task():
    return MyCustomObject('asd')


def use_object_task(obj):
    assert not obj.test_property_called
    assert obj.storage == 'asd'


def use_object_task_property(obj):
    assert obj.test_property_called
    assert obj.storage == 'asd'


def return_dict(obj):
    return {'obj': obj}


def myassert(a, b=None):
    if b is not None:
        assert a == b, (a, b)
    else:
        assert a


@tb.workflow
class MyModuleWorkflow:
    input_value = tb.var(default='xyz')
    input_default_classmethod = tb.var(default=MyCustomObject.create_an_object)
    input_default_instance = tb.var(default=MyCustomObject('987'))

    @tb.task
    def method_call_from_task(self):
        """test method_call_task"""

        return tb.node(
            self.return_object_task.test_function, a='A', b='B', c='C'
        )

    @tb.task
    def test_method_call_from_task(self):
        """test method_call_task"""

        return tb.node(
            myassert, a=self.method_call_from_task.storage, b='asdABC'
        )

    """
    These tests are disabled until we have workflow input serialization.
    @tb.task
    def method_call(self):
        return tb.node(
            self.input_default_instance.test_function, a='a', b='b', c='c'
        )

    @tb.task
    def test_method_call(self):
        return tb.node(myassert, a=self.method_call.storage, b='987abc')
    """

    @staticmethod
    def my_static_method(param):
        return param.storage

    @tb.task
    def static_method_call(self):
        """Test a simple call of self.static method as a node
        first argument."""
        return tb.node(self.my_static_method, param=self.return_object_task)

    @tb.task
    def test_static_method_call(self):
        """Verifies that the static method call returned correct output."""
        return tb.node(myassert, a=self.static_method_call, b='asd')

    @tb.task
    def class_method_as_a_task(self):
        """Calls class method as a task"""
        # XXX Changed from self.input_default_class.create_an_object.
        # Now self.input_default_classmethod is a pointer to the latter.
        # We have committed to encoding callable paths to standalone
        # functions and classmethods, but not actual class objects
        # which would be required for an inputvar to /be/ a class, and
        # this would prevent us from serializing the workflow.
        return tb.node(self.input_default_classmethod, storage='555')

    @tb.task
    def test_class_method_a_task(self):
        """Verifies the output of class_method_as_a_task -task."""
        return tb.node(
            myassert, a=self.class_method_as_a_task.storage, b='555'
        )

    @tb.task
    def direct_class_method_as_task(self):
        """Directly referencing a class method to a task"""
        return tb.node(MyCustomObject.create_an_object, storage='444')

    @tb.task
    def return_object_task(self):
        """Tests tb_encode, as the output will be saving a json-encoded
        version of the object."""

        return tb.node(return_object_task)

    @tb.task
    def use_object_task(self):
        """Tests tb_decode, as the input of this task will be loading the
        json encoded object."""

        return tb.node(use_object_task, obj=self.return_object_task)

    @tb.task
    def use_object_task_property(self):
        """Tests whether one can access properties of encoded/decoded
        objects at node definition time.
        """

        return tb.node(
            use_object_task_property,
            obj=self.return_object_task.test_property,
        )

    @tb.task
    def return_dict(self):
        """Returns a dictionary of a custom object, in order to test
        chaining of getitem and getattr at node definition time."""

        return tb.node(return_dict, obj=self.return_object_task)

    @tb.task
    def index_dict(self):
        """Makes sure that the property function was called, and subsequently
        also makes sure that chaining of getattr and getitems work."""

        return tb.node(
            myassert,
            a=self.return_dict['obj'].test_property.test_property_called,
        )

    @tb.task
    def test_call_function(self):
        """Tests a function call in a complicated pattern. Function call
        contains own get-item operations.
        """
        return tb.node(
            myassert,
            a=self.return_dict['obj']
            .test_function(self.return_object_task.storage, 'b', 'c')
            .storage,
            b='asdasdbc',
        )

    @tb.task
    def test_nested_function_call(self):
        """Test a nested function call. Calling test_function inside
        test function arguments.
        """
        return tb.node(
            myassert,
            a=self.return_object_task.test_function(
                a=self.return_object_task.test_function('a', 'b', 'c').storage,
                b=self.return_object_task.test_function('1', '2', '3').storage,
                c='x',
            ).storage,
            b='asdasdabcasd123x',
        )

    @tb.subworkflow
    def subworkflow_with_call(self):
        return SubworkflowWithCall(
            input_value=self.return_object_task.test_function('5', '6', '7')
        )

    @tb.task
    def test_task_in_subworkflow(self):
        """The subworkflow_with_call subworkflow has a testing task
        called test, which is launched here.
        """
        return tb.node('define', obj=self.subworkflow_with_call.test)

    @tb.task
    def call_subworkflow_return_value(self):
        """Subworkflow will return an object, which we reference here, and
        call function of this object."""

        return tb.node(
            myassert,
            a=self.subworkflow_with_call.return_object_subworkflow.test_function(
                '4', '5', '6'
            ).storage,
            b='asd456',
        )


@tb.workflow
class InnerSubworkflow:
    input = tb.var()

    @tb.task
    def testtask(self):
        return tb.node('define', obj=self.input)

    @tb.task
    def failing_task(self):
        """Expected to fail."""
        return tb.node(
            'taskblaster.testing.check_value', value=1, expected_value=2
        )


@tb.workflow
class PassThrough:
    a = tb.var()
    b = tb.var()

    @tb.task
    def A(self):
        return tb.node('define', obj=self.a)

    @tb.task
    def B(self):
        return tb.node('define', obj=self.b)


@tb.workflow
class SubworkflowWithSubworkflow:
    input = tb.var()

    @tb.subworkflow
    def sub(self):
        return InnerSubworkflow(input=self.input)


@tb.workflow
class SubworkflowAccessTest:
    @tb.subworkflow
    def second(self):
        return SubworkflowWithSubworkflow(input=self.first.sub.testtask)

    @tb.subworkflow
    def first(self):
        return SubworkflowWithSubworkflow(input=3)

    @tb.subworkflow
    def pass1(self):
        return PassThrough(a=1, b=self.pass2.A)

    @tb.subworkflow
    def pass2(self):
        return PassThrough(a=self.pass1.A, b=self.pass1.B)


def tb_init_repo(root):
    return Repository(root)
