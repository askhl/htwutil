import os
import sqlite3
from functools import cached_property
from pathlib import Path

import pytest
from click.testing import CliRunner

import taskblaster as tb
from taskblaster.cli import tb as tb_cli
from taskblaster.registry import Index, Registry
from taskblaster.repository import Repository


def pytest_report_header(config, start_path):
    from taskblaster.runner import TOPOLOGY_DEBUG

    if TOPOLOGY_DEBUG:
        yield 'Topology debug on (reversing task iterators)'


@pytest.fixture
def testdir(tmp_path):
    cwd = Path.cwd()
    try:
        os.chdir(tmp_path)
        yield tmp_path
    finally:
        os.chdir(cwd)
        print('tmp_path:', tmp_path)


# Port tests to use the tool fixture in most cases, so we do not need so
# many repository fixtures.
class Tool:
    def __init__(self, repo):
        self.repo = repo
        self.repo._tasks.update(_test_tasks)
        self.registry = self.repo.registry

    @property
    def simpleworkflow(self):
        return SimpleWorkflow

    @cached_property
    def _clirunner(self):
        return CliRunner()

    def workflow(self, workflow, add_workflows_to_registry=False):
        rn = self.repo.runner()
        with self.repo:
            rn.run_workflow(
                workflow,
                add_workflows_to_registry=add_workflows_to_registry,
                source='',
            )

    def workflow_function(self, func):
        rn = self.repo.runner()
        with self.repo:
            func(rn)

    def unrun(self, tree=None):
        if tree is None:
            tree = ['tree']

        init_counts = self.count()
        with self.repo:
            self.repo.tree(tree).select_unrun()[-1]()
        new_counts = self.count()

        kvp = [(key, init_counts[key], new_counts[key]) for key in init_counts]
        return {key.value: new - init for key, init, new in kvp}

    def remove(self, tree=None):
        if tree is None:
            tree = ['tree']

        init_counts = self.count()
        with self.repo:
            self.repo.tree(tree).remove()[-1]()
        new_counts = self.count()

        kvp = [(key, init_counts[key], new_counts[key]) for key in init_counts]
        return {key.value: new - init for key, init, new in kvp}

    def run(self, tree='tree', *args, **kwargs):
        return self.repo.run_worker(tree, *args, **kwargs)

    def kick(self, *args):
        return self.command(f'warden kick {" ".join(args)}').stdout

    def resolve_conflict(self, tree='tree'):
        with self.repo:
            self.repo.tree(tree).resolve_conflict()

    def command(self, string, check: bool = True):
        import shlex

        argv = shlex.split(string)
        result = self._clirunner.invoke(tb_cli, argv)
        print(result.stdout)
        if check and result.exit_code != 0:
            raise result.exception
        return result

    def ls(self, args=''):
        return self.command(f'ls {args}').stdout

    def _tree(self, *args, **kwargs):
        return self.repo.tree(*args, **kwargs, relative_to=self.repo.tree_path)

    def get_dependencies(self, name):
        from taskblaster.future import parent_state_info

        with self.repo:
            return parent_state_info(self.registry, name)[:2]

    def select(self, *args, **kwargs):
        with self.repo:
            return [*self._tree(*args, **kwargs).nodes()]

    def select_topological(self, *args, **kwargs):
        with self.repo:
            return [*self._tree(*args, **kwargs).nodes_topological()]

    def count(self, tree='tree', at_least=False, **kwargs):
        from taskblaster.state import State

        with self.repo:
            stat = self.repo.tree(tree).stat()

        states = {name: stat.counts[getattr(State, name)] for name in kwargs}
        if at_least:
            for kwarg in kwargs:
                assert states[kwarg] >= kwargs[kwarg]
        else:
            assert kwargs == states
        return stat.counts

    def input(self, name):
        with self.repo:
            return self.repo.cache[name]._actual_inputs

    def peek(self, name):
        with self.repo:
            return self.repo.cache[name]._actual_output

    def submit(self, string=''):
        return self.command(f'submit {string}')

    def clone(self, clonepath, subtree):
        self.command(f'special clone-sub-tree {clonepath} {subtree}')
        repo = Repository(clonepath)
        return Tool(repo)

    def task_hash(self):
        runner = CliRunner()
        result = runner.invoke(tb_cli, ['special', 'task-name-hash'])
        return result.stdout.strip()

    def where_conflict(self, state='c'):
        with self.repo:
            return [
                node.name
                for node in self.repo.tree().nodes()
                if self.registry.conflict_info(node.name).state.value == state
            ]

    def check_conflict(self, conflicts=None, resolved=None):
        if conflicts is not None:
            assert conflicts == len(self.where_conflict(state='c'))
        if resolved is not None:
            assert resolved == len(self.where_conflict(state='r'))

    def frozen_tasks(self):
        """
        Returns dict of frozen_tasks.
        key is name of tasks item is reasons for frozen
        """
        dct = {}
        for name, why in self.registry.frozentasks.select_all():
            dct.setdefault(name, []).append(why)
        return dct

    def get_failure(self, name) -> str:
        with self.repo:
            run_info = self.registry.workers.get_runinfo(name)
            if run_info:
                return run_info.exception
            return ''


@pytest.fixture
def tool(testdir):
    repo = Repository.create(testdir)
    return Tool(repo)


@pytest.fixture
def repo(tool):
    with tool.repo:
        yield tool.repo


@pytest.fixture
def simplerepo(tool):
    tool.repo._tasks.update(_test_tasks)
    tool.workflow(SimpleWorkflow(msg='hello'))
    return tool.repo


@pytest.fixture
def registry(tmp_path):
    return Registry(tmp_path / 'registry.dat')


@pytest.fixture
def namespace(tbdb):
    return tbdb.namespace


@pytest.fixture
def index(tmp_path):
    with sqlite3.connect(tmp_path / 'registry.db') as conn:
        conn.execute('PRAGMA foreign_keys = ON;')
        yield Index.initialize(conn)


# Task functions and workflows for simplerepo:
# (Maybe move to another module)
_test_tasks = {}


def taskfunc(func):
    _test_tasks[func.__name__] = func
    return func


@taskfunc
def fail(msg):
    raise RuntimeError(msg)


@taskfunc
def ok(msg):
    return msg


@tb.workflow
class Subworkflow:
    msg = tb.var()

    @tb.task
    def hello(self):
        return tb.node('ok', msg=self.msg)

    @tb.task
    def hello2(self):
        return tb.node('ok', msg=self.hello)

    @tb.task
    def fail(self):
        return tb.node('fail', msg=self.msg)

    @tb.task
    def dependsonfail(self):
        return tb.node('ok', msg=self.fail)


@tb.workflow
class SimpleWorkflow:
    msg = tb.var(default='hello')

    @tb.task
    def fail(self):
        return tb.node('fail', msg=self.msg)

    @tb.task
    def ok(self):
        return tb.node('ok', msg=self.msg)

    @tb.task
    def ok2(self):
        return tb.node('ok', msg=self.ok)

    @tb.task
    def forward_ref(self):
        return tb.node('ok', msg=self.dependsondependsonfail)

    @tb.task
    def dependsonfail(self):
        return tb.node('ok', msg=self.fail)

    @tb.task
    def dependsondependsonfail(self):
        return tb.node('ok', msg=self.dependsonfail)

    @tb.subworkflow
    def subworkflow(self):
        return Subworkflow(msg=self.ok2)

    @tb.task
    def dependsonsubworkflow(self):
        return tb.node('ok', msg=self.subworkflow.hello2)

    @tb.task
    def dependsonmany_failed(self):
        return tb.node(
            'ok',
            thing1=self.subworkflow.hello2,
            thing2=self.ok,
            thing3=self.ok2,
        )
